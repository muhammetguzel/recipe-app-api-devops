variable "prefix" {
  default = "raad"
}

variable "project" {
  default = "api-app-devops"
}

variable "contact" {
  default = "deneme@muhammetguzel.com"
}