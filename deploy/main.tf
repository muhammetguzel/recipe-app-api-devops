terraform {
  backend "s3" {
    bucket         = "api-app-tfstate"
    key            = "recipe-app.tfstate"
    region         = "us-east-1"
    encrypt        = true
    dynamodb_table = "api-app-tf-state-lock"
  }
}

provider "aws" {
  region  = "us-east-1"
  version = "~> 3.37"

}

locals {
  prefix = "${var.prefix}-${terraform.workspace}"
  common_tags = {
    Environment = terraform.workspace
    Project     = var.project
    Owner       = var.contact
    ManagedBy   = "Terraform"
  }
}

